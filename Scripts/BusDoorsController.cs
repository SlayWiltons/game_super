﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;

public class BusDoorsController : MonoBehaviour
{
    public bool opened;
    public bool closed;
    public float timeToClose;
    private splineMove busMove;    
    private GameObject Door_1A;
    private GameObject Door_1B;
    private GameObject Door_2A;
    private GameObject Door_2B;
    

    void Start ()
    {
        busMove = GameObject.Find("EXTE_BUS_STRUCTURE_SepratePartsBUSBUS").GetComponent<splineMove>();
        Door_1A = GameObject.Find("DOOR_1A");
        Door_1B = GameObject.Find("DOOR_1B");
        Door_2A = GameObject.Find("DOOR_2A");
        Door_2B = GameObject.Find("DOOR_2B");
        opened = false;
        closed = false;
    }

    void Update ()          
    {
        if (busMove.currentPoint == 0 || busMove.currentPoint == 7 || busMove.currentPoint == 12)
        {
            if (opened == false)
            {
                StopCoroutine(DelayToCloseDoors());//
                OpenDoors();
                opened = true;
                closed = false;
                StartCoroutine(DelayToCloseDoors());//
            }
        }
        else
        {
            if (closed == false)
            {
                CloseDoors();
                closed = true;                
            }
            opened = false;
        }
    }

    public void OpenDoors()
    {
        Door_1A.GetComponent<Animation>().Play("BusDoorOpen");
        Door_1B.GetComponent<Animation>().Play("BusDoorOpen2");
        Door_2A.GetComponent<Animation>().Play("BusDoorOpenDoor2A");
        Door_2B.GetComponent<Animation>().Play("BusDoorOpenDoor2B");
    }
    public void CloseDoors()
    {
        Door_1A.GetComponent<Animation>().Play("BusDoorClose");
        Door_1B.GetComponent<Animation>().Play("BusDoorClose2");
        Door_2A.GetComponent<Animation>().Play("BusDoorCloseDoor2A");
        Door_2B.GetComponent<Animation>().Play("BusDoorCloseDoor2B");
    }

    public IEnumerator DelayToCloseDoors()
    {
        StopCoroutine(DelayToCloseDoors());
        Debug.Log("Coroutine stoped");
        yield return new WaitForSeconds(0);

        Debug.Log("Coroutine started");
        yield return new WaitForSeconds(timeToClose);
        if (closed == false)
        {            
            CloseDoors();
            Debug.Log("Coroutine worked");
            closed = true;
        }
    }
}
