﻿using UnityEngine;
using System.Collections;

public class CloseBusDoor : MonoBehaviour {

	private float distance;
	private GameObject player;
	private GameObject buscamera;
	private Ray ray;
	private bool closed;
	private Camera camera;

	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		buscamera = GameObject.FindGameObjectWithTag ("BusCamera");
		camera = Camera.main;
		closed = false;

	}


	public void CloseTheDoorGo() {
		StartCoroutine(CloseTheDoor());
	}

	public IEnumerator CloseTheDoor() {

		//if (closed == false)

		//{
			
			yield return new WaitForSeconds (15.0f);
			gameObject.GetComponent<Animation> ().Play ("BusDoorClose");
			gameObject.GetComponent<Animation> ().Play ("BusDoorClose2");
			//closed = true;
			StopCoroutine(CloseTheDoor());
			Debug.Log ("DOORS CLOSED");
		//}

	}



	/*
	public void OpenTheDoor() {
		
		if (opened == false)

		{
			distance = Vector3.Distance(transform.position, player.transform.position);
			if (camera.isActiveAndEnabled) {
				ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			}
			RaycastHit hit;
			if (distance < 4 && Physics.Raycast (ray, out hit) && hit.collider.gameObject.tag == "BusDoor") {
				gameObject.GetComponent<Animation> ().Play ("BusDoorOpen");
				gameObject.GetComponent<Animation> ().Play ("BusDoorOpen2");
				opened = true;
				//GameObject.FindGameObjectWithTag ("Collider").SetActive (false);
				//GameObject.FindGameObjectWithTag("BusCamera").SetActive (true);
				//player.SetActive (false);
				//buscamera.SetActive (true);
				//GameObject.FindGameObjectWithTag("BusCamera").SetActive (true);



			}
		}

	}
	*/


	void Update () {



	}

}
