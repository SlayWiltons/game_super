﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpenerScript : MonoBehaviour {

	private GameObject item;
	public bool doorIsOpen = false;

	// Use this for initialization
	void Start () {
		item = gameObject;
	}


	public void openHomeDoor() {

		item.GetComponent<Animation> ().Play ("DoorOpen");
		doorIsOpen = true;
	}

	// Update is called once per frame
	void Update () {
		
	}
}
