﻿using SWS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubInstructions : MonoBehaviour
{
    private splineMove2 myMove;
    private GameObject player, playercamera;
    public GameObject Timers;
    private GameObject InstructionsCamera;
    public GameObject Instructions;
    public bool instr_flag;
    public GameObject Text_1;
    public GameObject Text_2;
    public GameObject Text_3;
    public GameObject Text_4;
    public GameObject Button_Next_1;
    public GameObject Button_Next_2;
    public GameObject Button_Next_3;
    public GameObject Button_Prev_1;
    public GameObject Button_Prev_2;
    public GameObject Button_Prev_3;
    public GameObject Button_Start;

    void Start ()
    {
        player = GameObject.Find("FPSController");
        playercamera = GameObject.FindGameObjectWithTag("MainCamera");
        myMove = GameObject.Find("FPSController").GetComponent<splineMove2>();
        InstructionsCamera = GameObject.FindGameObjectWithTag("InstructionsCamera");
        instr_flag = true;
    }
	
	void Update ()
    {
        if (myMove.pathContainer.name.Equals("FromBathToBus") && myMove.currentPoint == 5)
        {
            if (instr_flag == true)
            {
                if (Time.timeScale == 1)
                {
                    Time.timeScale = 0;
                    playercamera.SetActive(false);
                    InstructionsCamera.SetActive(true);
                    Instructions.SetActive(true);                    
                }
            }
        }
	}

    public void Next_1()
    {
        Text_1.SetActive(false);
        Text_2.SetActive(true);
        Button_Next_1.SetActive(false);
        Button_Next_2.SetActive(true);
        Button_Prev_1.SetActive(true);
    }

    public void Next_2()
    {
        Text_2.SetActive(false);
        Text_3.SetActive(true);
        Button_Next_2.SetActive(false);
        Button_Next_3.SetActive(true);
        Button_Prev_1.SetActive(false);
        Button_Prev_2.SetActive(true);
    }

    public void Next_3()
    {
        Text_3.SetActive(false);
        Text_4.SetActive(true);
        Button_Next_3.SetActive(false);
        Button_Prev_2.SetActive(false);
        Button_Prev_3.SetActive(true);
        Button_Start.SetActive(true);
    }

    public void Prev_1()
    {
        Text_2.SetActive(false);
        Text_1.SetActive(true);
        Button_Next_2.SetActive(false);
        Button_Next_1.SetActive(true);
        Button_Prev_1.SetActive(false);
    }

    public void Prev_2()
    {
        Text_3.SetActive(false);
        Text_2.SetActive(true);
        Button_Next_3.SetActive(false);
        Button_Next_2.SetActive(true);
        Button_Prev_2.SetActive(false);
        Button_Prev_1.SetActive(true);
    }

    public void Prev_3()
    {
        Text_4.SetActive(false);
        Text_3.SetActive(true);
        Button_Start.SetActive(false);
        Button_Next_3.SetActive(true);
        Button_Prev_3.SetActive(false);
        Button_Prev_2.SetActive(true);
    }

    public void Continue()
    {
        Instructions.SetActive(false);
        InstructionsCamera.SetActive(false);
        playercamera.SetActive(true);
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
        instr_flag = false;
        Text_1.SetActive(true);
        Button_Next_1.SetActive(true);
        Text_2.SetActive(false);
        Text_3.SetActive(false);
        Text_4.SetActive(false);
        Button_Next_2.SetActive(false);
        Button_Next_3.SetActive(false);
        Button_Prev_1.SetActive(false);
        Button_Prev_2.SetActive(false);
        Button_Prev_3.SetActive(false);
        Button_Start.SetActive(false);
    }
}
