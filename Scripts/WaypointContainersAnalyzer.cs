﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SWS;
using TurnTheGameOn.Timer;
using System;

[RequireComponent(typeof(AudioSource))]
public class WaypointContainersAnalyzer : MonoBehaviour 
{
    #region Values Description
    
	private splineMove2 myMove;
	private splineMove busMove;
	private GameObject buscamera, buscamerain;
	private HandsScript hands;
	private mqttTest mqttdata;
	private HttpDataScript httpdata;
	private AudioSource describeTakeItem; 
	private AudioSource describeComodo, describeComodoGlasses, describeComodoBag, describeComodoComplete; 
	private AudioSource describeCoatDoor, describeCoatDoorComplete;
	private AudioSource describeDoor, describeDoorComplete;
	private AudioSource describeOpenDoor;
	private AudioSource describeToBusStop;
	private AudioSource describeTakeMeshCart;
	private AudioSource describePayAtMarket;
	private AudioSource describeMeshCartToPacket;
	private AudioSource describeTakeMeshCartUp;
	private AudioSource describeBusStickHand;
	private AudioSource describeNotYourBusStop;
	private AudioSource describeTakeProducts;
    public AudioSource dogBarkingSound;
	private TakerScript sunglasses;
	private GameObject player, playercamera;
    private GameObject InstructionsCamera;

	/* Mesh carts */
	private GameObject meshcart, meshcartfull, meshcartdummy;

	/* Bus pay */
	private OpenBusDoor1A busPayDoor;

	private PhysicalTakerScript sunglassesPhysical, ballPhysical, bagPhysical, coatPhysical;
	private DoorOpenerScript doorPhysical;
	private static GeneralConfig config;
    public GameObject num_117;
    public GameObject num_904;
    public GameObject num_117_2;
    public GameObject num_904_2;
    public GameObject Warning;
    public GameObject bus;
    public bool way = false;
    public bool dog = false;
    public bool marketStop = false;
    public bool busPay = false;
    public bool busOutSignal = false;

    /* Playing audio */
    public bool playingAudioDescribeTakeItem = false;
	public bool playingAudioDescribeComodo = false;
	public bool playingAudioDescribeComodoGlasses = false;
	public bool playingAudioDescribeComodoBag = false;
	public bool playingAudioDescribeComodoComplete = false;
	public bool playingAudioDescribeCoatDoor = false;
	public bool playingAudioDescribeCoatDoorComplete = false;
	public bool playingAudioDescribeDoor = false;
	public bool playingAudioDescribeDoorComplete = false;
	public bool playingAudioDescribeOpenDoor = false;
	public bool playingAudioDescribeToBusStop = false;
	public bool playingAudioDescribeTakeMeshCart = false;
	public bool playingAudioDescribePayAtMarket = false;
	public bool playingAudioDescribeMeshCartToPacket = false;
	public bool playingAudioDescribeTakeMeshCartUp = false;
	public bool playingAudioDescribeTakeProducts = false;
	public bool playingAudioDescribeTakeProducts2 = false;

	/* Bus audio */
	public bool playingAudioDescribeBusStickHand = false;

	/* Played audio */
	public bool playedAudioDescribeTakeItem = false;
	public bool playedAudioDescribeComodo = false;
	public bool playedAudioDescribeComodoGlasses = false;
	public bool playedAudioDescribeComodoBag = false;
	public bool playedAudioDescribeComodoComplete = false;
	public bool playedAudioDescribeCoatDoor = false;
	public bool playedAudioDescribeCoatDoorComplete = false;
	public bool playedAudioDescribeDoor = false;
	public bool playedAudioDescribeDoorComplete = false;
	public bool playedAudioDescribeOpenDoor = false;
	public bool playedAudioDescribeToBusStop = false;
	public bool playedAudioDescribeTakeMeshCart = false;
	public bool playedAudioDescribePayAtMarket = false;
	public bool playedAudioDescribeMeshCartToPacket = false;
	public bool playedAudioDescribeTakeMeshCartUp = false;
	public bool playedAudioDescribeTakeProducts = false;
	public bool playedAudioDescribeTakeProducts2 = false;

	/* Bus audio */
	public bool playedAudioDescribeBusStickHand = false;
	public bool comodothingstaken = false;
	public bool coatthingstaken = false;
	public bool unlockingToMarket = false;
	public bool unlockingToHome = false;

	/* Physical items */
	private bool nearTheCoat = false;
	private bool coatIsTaken = false;

	/* Timers */
	private GameObject timerComodoTakes, timerComodoTakesBagMain, timerComodoTakesGlassesMain, timerYard, timerMarket, timerBus;
	private Timer timerComodoTakesBag;
	private Timer timerComodoTakesGlasses;
	private Timer timerInHome;
	private Timer timerComodoBag;
	private Timer timerYardMain;
	private Timer timerMarketMain;
    private Timer timerIntoBus;

	/* Delay Shit */
	private bool calledToGoToDoor = false;
	public string handsExecLimitDirection = "";
	public int handsExecLimit = 0;
    public GameObject BusStop_Checked;
    public GameObject Dog_Checked;
    public GameObject MarkeStop_Checked;
    private int tempStep = 0;
    #endregion

    #region voidStart
    void Start () 
	{        
        player = GameObject.Find ("FPSController");
		playercamera = GameObject.FindGameObjectWithTag ("MainCamera");
		myMove = GameObject.Find ("FPSController").GetComponent<splineMove2> ();
		busMove = GameObject.Find ("EXTE_BUS_STRUCTURE_SepratePartsBUSBUS").GetComponent<splineMove> ();
        buscamera = GameObject.FindGameObjectWithTag ("BusCamera");
        buscamerain = GameObject.FindGameObjectWithTag("BusCameraIn");
        hands = GameObject.Find ("FPSController").GetComponent<HandsScript> ();
        InstructionsCamera = GameObject.FindGameObjectWithTag("InstructionsCamera");

		/* Audio */
		describeTakeItem =  GameObject.Find ("TakeItem").GetComponent<AudioSource> ();
		describeComodo = GameObject.Find ("DescribeComodo").GetComponent<AudioSource> ();
		describeComodoComplete = GameObject.Find ("DescribeComodoComplete").GetComponent<AudioSource> ();
		describeComodoGlasses = GameObject.Find ("DescribeComodoGlasses").GetComponent<AudioSource> ();
		describeComodoBag = GameObject.Find ("DescribeComodoBag").GetComponent<AudioSource> ();
		describeCoatDoor = GameObject.Find ("DescribeCoatDoor").GetComponent<AudioSource> ();
		describeCoatDoorComplete = GameObject.Find ("DescribeCoatDoorComplete").GetComponent<AudioSource> ();
		describeNotYourBusStop = GameObject.Find ("DescribeNotYourBusStop").GetComponent<AudioSource> ();
		describeDoor = GameObject.Find ("DescribeDoor").GetComponent<AudioSource> ();
		describeOpenDoor = GameObject.Find ("DescribeOpenDoor").GetComponent<AudioSource> (); // door opener
		describeToBusStop = GameObject.Find ("DescribeToBusStop").GetComponent<AudioSource> ();
		describeTakeMeshCart = GameObject.Find ("DescribeTakeMeshCart").GetComponent<AudioSource> ();
		describePayAtMarket = GameObject.Find ("DescribePayAtMarket").GetComponent<AudioSource> ();
		describeMeshCartToPacket = GameObject.Find ("DescribeMeshCartToPacket").GetComponent<AudioSource> ();
		describeTakeMeshCartUp = GameObject.Find ("DescribeTakeMeshCartUp").GetComponent<AudioSource> ();
		describeTakeProducts = GameObject.Find ("DescribeTakeProducts").GetComponent<AudioSource> ();

		/* Bus Audio */
		describeBusStickHand =  GameObject.Find ("DescribeBusStickHand").GetComponent<AudioSource> ();

		/* Mqtt Data */
		mqttdata = GameObject.Find ("FPSController").GetComponent<mqttTest>();
		sunglasses = GameObject.Find ("mH_Sunglasses").GetComponent<TakerScript>();
		doorPhysical = GameObject.FindGameObjectWithTag ("DoorHome").GetComponent<DoorOpenerScript>();
		config = new GeneralConfig ();

		/* Http Data */
		httpdata = GameObject.Find ("FPSController").GetComponent<HttpDataScript>();

		/* Mesh carts */
		meshcart = GameObject.FindGameObjectWithTag ("MeshCartFPS");
		meshcartfull = GameObject.FindGameObjectWithTag ("MeshCartFPSFull");
		meshcartdummy = GameObject.FindGameObjectWithTag ("MeshCartDummy");

		// disable mesh cart views
		meshcart.SetActive (false);
		meshcartfull.SetActive (false);

		/* Physical items */
		sunglassesPhysical = GameObject.Find ("mH_Sunglasses").GetComponent<PhysicalTakerScript>();
		ballPhysical = GameObject.Find ("mH_Baseball").GetComponent<PhysicalTakerScript>();
		bagPhysical = GameObject.Find ("Bag").GetComponent<PhysicalTakerScript>();
		busPayDoor = GameObject.FindGameObjectWithTag ("BusDoor").GetComponent<OpenBusDoor1A>();

		/* Timers */
		// Home
		timerComodoTakes = GameObject.Find("TimerComodoTakes");
		timerInHome = GameObject.FindGameObjectWithTag ("TimerComodo").GetComponent<Timer>();
		timerInHome.StopTimer ();

		// Bag
		timerComodoTakesBagMain = GameObject.Find("TimerComodoTakesBagMain");
		timerComodoTakesBag = GameObject.Find("TimerComodoTakesBagMain").GetComponent<Timer>();
		timerComodoTakesBag.StopTimer ();

		// Glasses
		timerComodoTakesGlassesMain = GameObject.Find("TimerComodoTakesGlassesMain");
		timerComodoTakesGlasses = GameObject.Find("TimerComodoTakesGlassesMain").GetComponent<Timer>();
		timerComodoTakesGlasses.StopTimer ();

		// Yard
		timerYard = GameObject.Find("TimerYard");
		timerYardMain = GameObject.FindGameObjectWithTag ("TimerYardMain").GetComponent<Timer>();
		timerYardMain.StopTimer ();

        // IntoBus
        timerBus = GameObject.Find("TimerIntoBus");
        timerIntoBus = GameObject.FindGameObjectWithTag("TimerBus").GetComponent<Timer>();

        // Market
        timerMarket = GameObject.Find("TimerMarket");
		timerMarketMain = GameObject.FindGameObjectWithTag ("TimerMarketMain").GetComponent<Timer>();
		timerMarketMain.StopTimer ();
		timerComodoTakes.SetActive (false);
		timerComodoTakesBagMain.SetActive (false);
		timerComodoTakesGlassesMain.SetActive (false);
		timerYard.SetActive (false);
		timerMarket.SetActive (false);
        timerBus.SetActive(false);
		myMove.setStartedPath ("FromHomeToStairs");//HomeToMarket1
        hands.hideHands();
    }
    #endregion

    // Home to Market Scenario

    #region HomeToMarketSounds
    public void containerHomeToMarketSoundsAnalyzer() 
	{
		if (myMove.currentPathContainer ().name.Equals ("HomeToMarket1")) 
		{
			// Turn hands off 
			if (myMove.currentPoint != config.WAYPOINT_COMODO && myMove.currentPoint != config.WAYPOINT_COAT) 
			{
				showHands(false);
			}
			// Возле комода
			if (myMove.currentPoint == config.WAYPOINT_COMODO) 
			{
				showHands(true); 	// Show hands
				setHandsExecLimit("down", config.WAYPOINT_COMODO_LEFT_HAND_TAKE_LIMIT);
				//playSound("describeComodoBag"); 			// Describe glasses take
				StartCoroutine(playDelayedDescibeComodoBag()); //  Describe delayed glasses take
				if (comodothingstaken) 
				{ 								
					showHands(false); 									// Hide hands
					// Show some UI complete mark
				} // end comodo things taken
			}				
			// Возле двери с курткой
			if (myMove.currentPoint == config.WAYPOINT_COAT) 
			{
				showHands (true);
				playSound ("describeCoatDoor"); 						// Describe coat take
				if (coatthingstaken) 
				{
					showHands(false); 									// Hide hands
					// Show some UI complete mark
				} // end coat things taken
			}
			if (myMove.currentPoint == config.WAYPOINT_DOOR) 
			{
				playSound ("describeDoor"); 						// Describe door
			}
			if (myMove.currentPoint == config.WAYPOINT_AFTER_DOOR) 
			{
				playSound ("describeToBusStop"); 						// Describe to bus stop
			}
		}
	}
    #endregion

    #region MarketstopToMarketSounds
    public void containerFromMarketStopToMarketSoundsAnalyzer() 
	{
		if (myMove.currentPathContainer ().name.Equals ("FromMarketStopToMarket")) 
		{ 		
			if (myMove.currentPoint == 29) 
			{
				playSound ("describeMeshCartToPacket"); 	// Describe
				StartCoroutine(podnemitePoruchenGo()); 		// Describe after
				StartCoroutine(payPokupkiGo()); // Describe pay
			}
		}
	}
    #endregion

    public void containerManualFromMyMove(PathManager pathContainer, int index)
    {
        // Home To Bath 
        #region HomeToBath
        if (pathContainer.name.Equals ("HomeToBath")) 
		{
            meshcart.SetActive(false);
            if (index != 7) 
			{
				myMove.Pause (0.0f);	
			}
			if (index == 7) 
			{
				myMove.SetPath (WaypointManager.Paths["FromHomeToStairs"]);  
            }
		}
        #endregion

        // From Bath To Bus
        #region FromBathToBus
        if (pathContainer.name.Equals ("FromBathToBus")) 
		{			
            hands.hideHands();
            meshcartfull.SetActive(false);
            meshcart.SetActive(false);
            if (index == 3)    
            {
                busMove.startPoint = config.WAYPOINT_BUS_SUB_STOP;  //размещаем автобус на углу
                busMove.StartMove();
            }
            if (index == 5)
            {
                myMove.ChangeSpeed(0.7f);
            }
            if (index == 7)
            {
                busMove.startPoint = config.WAYPOINT_BUS_SUB_STOP;  //размещаем автобус на углу
                busMove.StartMove();
            }
            if (index == 13)
            {
                busMove.startPoint = config.WAYPOINT_BUS_SUB_STOP;  //размещаем автобус на углу
                busMove.StartMove();
            }
            if (index == 17)
            {
                busMove.startPoint = config.WAYPOINT_BUS_SUB_STOP;  //размещаем автобус на углу
                busMove.StartMove();
            }
            if (index == 18) 
			{
                // Стопарим игрока, пока не подьедет автобус
                myMove.Pause (0.0f);
				myMove.moveLock = true;
			}
            if (index == 20)
            {
                busPay = false;
                Debug.Log("Redirect Bus Flag");
            }
            // Stop IntoBus Timer
            if (index != 18 && index != 19 && busMove.currentPoint == config.WAYPOINT_BUS_HOME_STOP)
            {
                timerIntoBus.StopTimer();
            }
            if (myMove.moveLock == true)
            {
				Debug.Log ("MoveLock is true, stop trademill");
				mqttdata.sendMqtt ("stop_trademill");
			} 
		}
        #endregion

        // Home To Market 1
        #region HomeToMarket
        if (pathContainer.name.Equals ("HomeToMarket1")) 
		{
            meshcartfull.SetActive(false);
            meshcart.SetActive(false);
            if (index < 2) 
			{
				myMove.Resume ();
				Debug.Log ("auto move forward to comodo");
			}
			if (index >= 3) 
			{
				myMove.Pause (0.0f);
			}
			if (index == 3) 
			{
				myMove.ChangeSpeed (3);
				timerComodoTakes.SetActive (true);
				timerInHome.StartTimer ();
			}
			if (index == 5) 
			{
				myMove.ChangeSpeed (2);
			}
			if (index == 13) 
			{
				myMove.Pause (0.0f);
				myMove.SetPath (WaypointManager.Paths["FromHomeToStairs"]);
				myMove.GoToWaypoint (0);
			}
		}
        #endregion

        // From Home To Stairs
        #region HomeToStairs
        if (pathContainer.name.Equals("FromHomeToStairs"))         
        {
            if (index == 0)
            {
                hands.hideHands();
                showHands(false);
                meshcartfull.SetActive(false);
                meshcart.SetActive(false);
                timerInHome.StopTimer();
                // Start timer on Yard
                timerYard.SetActive(true);
                timerYardMain.StartTimer();
                myMove.ChangeSpeed(1.5f);
            }
            if (index == 7)
            {
                Debug.Log("End of Stairs");
                myMove.SetPath(WaypointManager.Paths["FromBathToBus"]);
                myMove.GoToWaypoint(0);
                myMove.ChangeSpeed(1.5f);
            }
        }
        #endregion

        // From Marketstop To Market
        #region FromMarketStopToMarket
        if (pathContainer.name.Equals ("FromMarketStopToMarket"))  
		{
            ///////////////////////////////////Добавить флаг об услышанной остановке
            if (index == 0) 
			{
				Debug.Log ("Reloading camera view");
				buscamera.SetActive (false);
				playercamera.SetActive (true);
                myMove.ChangeSpeed(1.5f);
                showHands(false);   //убираем руки и корзину (при переходе по меню)
                meshcartfull.SetActive(false);
                meshcart.SetActive(false);
			}
			if (index >= 10 && index < 33) 
			{
				myMove.Pause ();
			}
			if (index == 10) 
			{
                //Stop Yard Timer
                showHands(false);
                meshcartfull.SetActive(false);
                timerYardMain.StopTimer();
				// Start Timer At Market
				timerMarket.SetActive(true);
				timerMarketMain.StartTimer ();
			}
			if (index == 12) 
			{
                showHands(true);
				meshcartdummy.SetActive (false);
				meshcart.SetActive (true);
			}
			// Выбор продуктов
			if (index == 20) 
			{
				mqttdata.sendMqtt ("stop_trademill");
			}
			if (index == 21) 
			{
				meshcart.SetActive (false);
				meshcartfull.SetActive (true);
			}
			// Выбор продуктов 2
			if (index == 26) 
			{
				mqttdata.sendMqtt ("stop_trademill");
			}
			if (index == 33) 
			{
				myMove.Resume ();
			}
			// Оплата покупок
			if (index == 29) 
			{
				mqttdata.sendMqtt ("stop_trademill");
			}
			if (index == 29) 
			{
				myMove.moveLock = true;
			}
			if (index == 30) 
			{
				myMove.Resume ();
                showHands(false);
                meshcart.SetActive(false);
                meshcartfull.SetActive(false);
            }
			if (index == 39) 
			{
				myMove.Pause ();
			}
		}
        #endregion

        // From Bus To Home
        #region FromBusToHome
        if (pathContainer.name.Equals ("FromBusToHome")) 
		{
            meshcart.SetActive(false);
            meshcartfull.SetActive(false);
            if (index == 0) 
			{
				Debug.Log ("Reloading camera view");
				buscamera.SetActive (false);
				playercamera.SetActive (true);
			}
			if (index >= 4) 
			{
				myMove.Pause ();
			}
			if (index == 4) 
			{
				mqttdata.sendMqtt ("start_trademill");
			}
		}
        if (pathContainer.name.Equals("To3Busstop"))
        {
            if (index == 1)
            {
                Warning.SetActive(true);
            }
            if (index == 3)
            {
                busMove.startPoint = config.WAYPOINT_BUS_SUB_STOP;  //размещаем автобус на углу
                busMove.StartMove();
                StartCoroutine(ComeBack());
            }
        }
        #endregion
    }

    //Звуки
    #region PlaySounds
    public void playSound(string sound) 
	{
		if (sound == "describeTakeItem") // Describe pay at market
		{ 
			if (!describeTakeItem.isPlaying && !playingAudioDescribeTakeItem) 
			{
				describeTakeItem.Play();
				playingAudioDescribeTakeItem = true;
                playingAudioDescribeTakeItem = false;
            }
		}
		if (sound == "describeComodo") 
		{
			if (!describeComodo.isPlaying && !playingAudioDescribeComodo) // Describe glasses take and other
			{ 
				playingAudioDescribeComodo = true;
			}
		}
		if (sound == "describeComodoGlasses") 
		{
			if (!describeComodoGlasses.isPlaying && !playingAudioDescribeComodoGlasses) // Describe glasses
			{ 
				playingAudioDescribeComodoGlasses = true;
			}
			if (playingAudioDescribeComodoGlasses && !describeComodoGlasses.isPlaying) 
			{
				playedAudioDescribeComodoGlasses = true;
			}
		}
		if (sound == "describeComodoBag") 
		{
			if (!describeComodoBag.isPlaying && !playingAudioDescribeComodoBag) // Describe bag
			{ 
				playingAudioDescribeComodoBag = true;
			}
			if (playingAudioDescribeComodoBag && !describeComodoBag.isPlaying) 
			{
				playedAudioDescribeComodoBag = true;
			}
		}
		if (sound == "describeComodoComplete") 
		{
			if (!describeComodoComplete.isPlaying && !playingAudioDescribeComodoComplete) // Describe comodo complete
			{ 
				playingAudioDescribeComodoComplete = true;
			}
		}
		if (sound == "describeCoatDoor") // Describe coat take
		{ 
			if (describeComodoComplete.isPlaying) 
			{
				describeComodo.Stop();
			}
			if (!describeCoatDoor.isPlaying && !playingAudioDescribeCoatDoor) 
			{
				playingAudioDescribeCoatDoor = true;
			}
		}
		if (sound == "describeCoatDoorComplete") { // Describe coat take complete
			if (describeCoatDoor.isPlaying) 
			{
				describeCoatDoor.Stop();
			}
			if (!describeCoatDoorComplete.isPlaying && !playingAudioDescribeCoatDoorComplete) 
			{
				playingAudioDescribeCoatDoorComplete = true;
			}
		}
		if (sound == "describeDoor") // Describe coat take
		{ 
			if (describeCoatDoor.isPlaying) 
			{
				describeCoatDoor.Stop();
			}
			if (!describeDoor.isPlaying && !playingAudioDescribeDoor) 
			{
				playingAudioDescribeDoor = true;
			}
		}
		if (sound == "describeOpenDoor") // Describe coat take
		{ 
			if (describeDoor.isPlaying) 
			{
				describeDoor.Stop();
			}
			if (!describeOpenDoor.isPlaying && !playingAudioDescribeOpenDoor) 
			{
				playingAudioDescribeOpenDoor = true;
			}
		}
		if (sound == "describeToBusStop") // Describe coat take
		{ 
			if (describeDoor.isPlaying) 
			{
				describeDoor.Stop();
			}
			if (!describeToBusStop.isPlaying && !playingAudioDescribeToBusStop) 
			{
				playingAudioDescribeToBusStop = true;
			}
		}
		if (sound == "describeTakeMeshCart") // Describe mesh cart
		{ 
			if (!describeTakeMeshCart.isPlaying && !playingAudioDescribeTakeMeshCart) 
			{
				playingAudioDescribeTakeMeshCart = true;
			}
		}
		if (sound == "describeMeshCartToPacket") // Describe pay at market
		{ 
			if (!describeMeshCartToPacket.isPlaying && !playingAudioDescribeMeshCartToPacket) 
			{
				playingAudioDescribeMeshCartToPacket = true;
			}
		}
		if (sound == "describeTakeMeshCartUp") // Describe pay at market
		{ 
			if (!describeTakeMeshCartUp.isPlaying && !playingAudioDescribeTakeMeshCartUp) 
			{
				playingAudioDescribeTakeMeshCartUp = true;
			}
		}
		if (sound == "describePayAtMarket") // Describe pay at market
		{ 
			if (!describePayAtMarket.isPlaying && !playingAudioDescribePayAtMarket) 
			{
				playingAudioDescribePayAtMarket = true;
			}
		}
		if (sound == "describeBusStickHand") // Describe pay at market  
        { 
			if (!describeBusStickHand.isPlaying && !playingAudioDescribeBusStickHand) 
			{
                playingAudioDescribeBusStickHand = true;
            }
		}
		if (sound == "describeTakeProducts") // Describe pay at market
		{ 
			if (!describeTakeProducts.isPlaying && !playingAudioDescribeTakeProducts) 
			{
				playingAudioDescribeTakeProducts = true;
			}
		}	
		if (sound == "describeTakeProducts2") // Describe pay at market
		{ 
			if (!describeTakeProducts.isPlaying && !playingAudioDescribeTakeProducts2) 
			{
				playingAudioDescribeTakeProducts2 = true;
			}
		}
	}
    #endregion

    #region StopSound
    public void stopSound(string sound) 
	{
		if (sound == "all") 
		{
			if (describeCoatDoorComplete.isPlaying) 
			{
				describeCoatDoorComplete.Stop();
			}
		}
	}
    #endregion

    /* Delayed Audio Coroutines */
    #region DelaySounds
    IEnumerator playDelayedDescibeComodoBag()
	{
		yield return new WaitForSeconds(2);
		playSound ("describeComodoBag");
		StopCoroutine (playDelayedDescibeComodoBag());
	}
	IEnumerator playDelayedDescibeComodoGlasses()
	{
		yield return new WaitForSeconds(2);
		playSound ("describeComodoGlasses");
		StopCoroutine (playDelayedDescibeComodoGlasses());
	}		
	IEnumerator podnemitePoruchenGo()
	{
		yield return new WaitForSeconds(25);    /////////////////////////////////////////////////////
		playSound ("describeTakeMeshCartUp");
		StopCoroutine (podnemitePoruchenGo());
	}
	IEnumerator payPokupkiGo()
	{
		yield return new WaitForSeconds(30);
		playSound ("describePayAtMarket");
		StopCoroutine (payPokupkiGo());
	}
    #endregion

    #region ShowHands
    public void showHands(bool show) 
	{
		if (show) {
			if (hands.handsShowed == false) 
			{
				hands.showHands ();
			}
		} 
		else 
		{
			if (hands.handsShowed == true) 
			{
				hands.hideHands ();
			}
		}
	}
    #endregion

    #region containerHomeToMarketHandsAnalyzer
    /*public void containerHomeToMarketHandsAnalyzer() 
	{
		if (myMove.currentPathContainer().name.Equals("HomeToMarket1")) 
		{
			// hands 
			if (myMove.currentPoint != 3 && myMove.currentPoint != 9) 
			{
				hands.hideHands ();
			}
			if (myMove.currentPoint == 3 && hands.handsShowed == false) 
			{
				hands.showHands ();
			}
			if (myMove.currentPoint == 9 && hands.handsShowed == false) 
			{
				hands.showHands ();
			}
		}
	}*/
    #endregion

    // Step analyze and lock
    #region containerMoveStepAnalyzer
    public void containerMoveStepAnalyzer() 
	{
		//Debug.Log ("Container Move Step Analyzer Hello");
		if (mqttdata.step == 1 && tempStep == 0 && myMove.getMoveLock () == false) 
		{
			tempStep = 1;
			myMove.Resume();
		}
		if (mqttdata.step == 0 && tempStep == 1 && myMove.getMoveLock () == false) 
		{
			tempStep = 0;
		}
		if (mqttdata.step == 1 && myMove.getMoveLock () == true) 
		{
			playSound ("describeNotYourBusStop");
		}
	}
    #endregion

    #region HandsTakerAnalyzer
    public void HandsTakerAnalyzer() 
	{
		if (handsExecLimitDirection == "down" && (mqttdata.leftarmdegree >= handsExecLimit || mqttdata.rightarmdegree >= handsExecLimit)) 
		{
			// activate taker
			if (mqttdata.lefttaker == 1) 
			{
				sunglasses.takeleft ();
				Debug.Log ("Down - left take");
			}
			if (mqttdata.righttaker == 1) 
			{
				sunglasses.takeright ();
				Debug.Log ("Down - right take");
			}
		}
		if (handsExecLimitDirection == "up" && (mqttdata.leftarmdegree <= handsExecLimit || mqttdata.rightarmdegree <= handsExecLimit)) 
		{
			// activate taker
			if (mqttdata.lefttaker == 1) 
			{
				sunglasses.takeleft ();
				Debug.Log ("UP - left take");
			}
			if (mqttdata.righttaker == 1) 
			{
				sunglasses.takeright ();
				Debug.Log ("UP - right take");
			}
		}
	}
    #endregion

    #region setHandsExecLimit
    public void setHandsExecLimit(string direction, int value) 
	{
		handsExecLimit = value;
		handsExecLimitDirection = direction;
	}
    #endregion

    // BUS STOPS/LOCK
    #region containerBusStopsAnalyzer
    public void containerBusStopsAnalyzer()     /////////////////////////////////////////////////////
	{
		if(myMove.getStartedPath().Equals("HomeToSwimmingPool") && buscamera.activeSelf) 
		{
			// unlock go, if swimming pool stop
			if(busMove.currentPoint == config.WAYPOINT_BUS_POOL_STOP) 
			{
				myMove.setMoveLock (false);
			}
			// lock go, if supermarket stop
			// ignore steps
			if (busMove.currentPoint == config.WAYPOINT_BUS_MARKET_STOP) 
			{
				myMove.setMoveLock (true);
			}
		}
		if (myMove.getStartedPath ().Equals ("HomeToMarket1") && buscamera.activeSelf) 
		{
			// lock go, if swimming pool stop
			// ignore steps
			if(busMove.currentPoint == config.WAYPOINT_BUS_POOL_STOP) 
			{
				myMove.setMoveLock (true);
			}
			// unlock go, if supermarket stop
			if (busMove.currentPoint == config.WAYPOINT_BUS_MARKET_STOP /*&& unlockingToMarket == false*/) ////убрать анлокинг магазина!!!
			{
				unlockingToMarket = true;
				Debug.Log ("We are on the market stop");
				myMove.setMoveLock (false);
				Debug.Log ("Unlocked move");
				busPayDoor.inbus = false;
				busPayDoor.cameraSetIntoBus = false;
				busPayDoor.payed = false;
				mqttdata.buspay = 0;
				myMove.SetPath (WaypointManager.Paths["FromMarketStopToMarket"]);
				myMove.GoToWaypoint (0);
				myMove.Resume ();
				/* unlock to home then */
				unlockingToHome = true;
				Debug.Log ("Moved to point 0");
			}
			if (busMove.currentPoint == config.WAYPOINT_BUS_HOME_STOP && unlockingToHome == true) 
			{
				unlockingToHome = false;
				busPayDoor.inbus = false;
				busPayDoor.cameraSetIntoBus = false;
				busPayDoor.payed = false;
				myMove.SetPath (WaypointManager.Paths["FromBusToHome"]);
				myMove.GoToWaypoint (0);
				myMove.Resume ();
			}
		}
	}
    #endregion

    #region PhysicalTakerAnalyzer
    void PhysicalTakerAnalyzer() 
	{
		if (myMove.currentPathContainer ().name.Equals ("HomeToMarket1")) 
		{
			if (myMove.currentPoint == config.WAYPOINT_COMODO) 
			{
				// start timer once
				if (playedAudioDescribeComodoBag) 
				{
					if (!timerComodoTakesBagMain.activeSelf) 
					{
						timerComodoTakesBagMain.SetActive (true);
						timerComodoTakesBag.StartTimer ();
					}
				}
				if (playedAudioDescribeComodoGlasses) 
				{
					// start timer once
					if (!timerComodoTakesGlassesMain.activeSelf) 
					{
						timerComodoTakesGlassesMain.SetActive (true);
						timerComodoTakesGlasses.StartTimer ();
					}
				}					
				if (mqttdata.ball == 1) 
				{
					ballPhysical.takethisitem ();
				}
				if (mqttdata.bag == 1) 
				{
					bagPhysical.takethisitem ();
					// stop bag timer and send data
					timerComodoTakesBag.StopTimer();
					httpdata.sendTimerComodoBag (timerComodoTakesBag.GetTimerValueSeconds());
					// start audio glasses
					StartCoroutine(playDelayedDescibeComodoGlasses()); //  Describe delayed glasses take
				}
				if (mqttdata.glasses == 1) 
				{
					sunglassesPhysical.takethisitem ();
					// stop glasses timer and send data
					timerComodoTakesGlasses.StopTimer();
					httpdata.sendTimerComodoGlasses (timerComodoTakesGlasses.GetTimerValueSeconds());
				}

				if (mqttdata.coat == 1) 
				{
					// WARN USER
					mqttdata.coat = 0;
				}

				if (mqttdata.door == 1) 
				{
					// WARN USER
					mqttdata.door = 0;
				}
			}
			if (myMove.currentPoint == config.WAYPOINT_COAT) 
			{
				nearTheCoat = true;
				if (mqttdata.coat == 1) 
				{
					coatPhysical.takethisitem ();
					coatIsTaken = true;
				}
				if (mqttdata.door == 1) 
				{
					// WARN USER
					mqttdata.door = 0;
				}
			}
			if (sunglassesPhysical.taken && bagPhysical.taken && nearTheCoat == false) // && ballPhysical.taken
			{ 
				myMove.moveLock = false;
				myMove.Resume ();
			}
			if (coatIsTaken == true && myMove.currentPoint != 11) 
			{
				Debug.Log ("Still calling coat");
				// Only once!
				if (!calledToGoToDoor) 
				{
					StartCoroutine (continueToGoToDoor ());
				}
			}
			if (myMove.currentPoint == 10) 
			{
				myMove.Resume ();
			}
			if (mqttdata.door == 1 && doorPhysical.doorIsOpen == false) 
			{
				doorPhysical.openHomeDoor ();
				playSound ("describeOpenDoor");
				StartCoroutine(continueToGo());
			}
		}
		if (myMove.currentPathContainer ().name.Equals ("FromMarketStopToMarket")) 
		{
			if (myMove.currentPoint == 29) 
			{
				if (mqttdata.buspay == 1) 
				{
					meshcartfull.SetActive (false);
                    showHands(false);
					playSound("describeTakeItem");
					myMove.Resume ();
					timerMarketMain.StopTimer ();
				}
			}
		}
			
		/* Bus */
		if ((mqttdata.buspay == 1 || Input.GetMouseButtonDown(1)) && busPayDoor.inbus && !busPayDoor.payed) // pay once
		{
            if (busPay == false)
            {
                busPayDoor.payed = true;
                playSound("describeTakeItem");
                Debug.Log("I payed");
                mqttdata.buspay = 0;    /////////////////////////////////////////////////
                buscamerain.SetActive(false);
                playercamera.SetActive(true);
                myMove.SetPath(WaypointManager.Paths["InsideTheBus"]);/////////////////////////////////////////////
                myMove.GoToWaypoint(0);
                myMove.ChangeSpeed(2);
            }
        }
        if (myMove.currentPathContainer().name.Equals("InsideTheBus"))///////////////////////////////////////////
        {
            if (myMove.currentPoint == 8)
            {
                playercamera.SetActive(false);
                buscamera.SetActive(true);
              //  busMove.Pause(5.0f);
            }
        }
	}
    #endregion

    /* Till door delay */
    IEnumerator continueToGoToDoor()
	{
		calledToGoToDoor = true;
		yield return new WaitForSeconds(12);
		myMove.Resume ();
		StopCoroutine (continueToGoToDoor());
	}
	IEnumerator continueToGo()
	{
		yield return new WaitForSeconds(1);
		myMove.Resume ();
		StopCoroutine (continueToGo());
	}
    IEnumerator ComeBack()
    {
        yield return new WaitForSeconds(3);
        myMove.SetPath(WaypointManager.Paths["FromBathToBus"]);
        myMove.GoToWaypoint(15);
        Warning.SetActive(false);
    }

    void BusSwitcher()
    {
        if (busMove.currentPoint > 2 && busMove.currentPoint < 15 && myMove.pathContainer.name == "FromBathToBus" && myMove.currentPoint == 18) //Заменяем автобус
        {
            num_117.SetActive(false);
            num_904.SetActive(true);
            num_117_2.SetActive(false);
            num_904_2.SetActive(true);
            busMove.startPoint = config.WAYPOINT_BUS_SUB_STOP;
            busMove.StartMove();
        }
        if (busMove.currentPoint == 0 && myMove.pathContainer.name == "FromBathToBus" && myMove.currentPoint == 19 && num_117.activeSelf == true)  //Блокируем возможность захода в ненужный автобус
        {
            myMove.GoToWaypoint(18);
        }
        if (busMove.currentPoint != 0 && myMove.currentPoint == 19)  //Блокируем возможность подъема в автобус при отсутствии такового
        {
            myMove.GoToWaypoint(18);
        }
    }

    void BusSpeedChanger()//поменять скорость при движении от бассейна
    {
        if (busMove.currentPoint == 0)
        {
            busMove.ChangeSpeed(15);
        }
        if (busMove.currentPoint == 18)
        {
            busMove.ChangeSpeed(5);
        }
        if (busMove.currentPoint == 7)
        {
            busMove.ChangeSpeed(6);
        }
    }

    public void MakeSignal()
    {
        if (way == false)
        {
            if (myMove.pathContainer.name == "FromBathToBus" && myMove.currentPoint >= 14 && myMove.currentPoint <= 18 && (Input.GetMouseButtonDown(1) || mqttdata.button == 1))
            {
                way = true;
                busMove.startPoint = config.WAYPOINT_BUS_SUB_STOP;
                busMove.StartMove();
                Debug.Log("Get signal");
                mqttdata.button = 0;
            }
        }
        if(myMove.pathContainer.name == "FromBathToBus" && myMove.currentPoint == 18 && way == false)
        {
            myMove.SetPath(WaypointManager.Paths["To3Busstop"]);
            myMove.GoToWaypoint(0);
            Debug.Log("Root changed");
        }
        if (myMove.pathContainer.name == "FromBathToBus" && myMove.currentPoint == 0)
        {
            busMove.startPoint = config.WAYPOINT_BUS_SUB_STOP;
            busMove.StartMove();
        }
        if (myMove.pathContainer.name == "FromBathToBus" && myMove.currentPoint >= 9 && myMove.currentPoint < 12 && (Input.GetMouseButtonDown(1) || mqttdata.button == 1))
        {
            Debug.Log("Get wrong signal");
            Warning.SetActive(true);
            mqttdata.button = 0;
        }
        if (myMove.pathContainer.name == "FromBathToBus" && myMove.currentPoint == 13 && Warning.activeSelf == true)
        {
            Warning.SetActive(false);
        }
        if (busMove.currentPoint >= 7 && busMove.currentPoint <= 11)
        {
            if (Input.GetMouseButtonDown(1) || mqttdata.button == 1)
            {
                marketStop = true;
                mqttdata.button = 0;
            }
        }
    }

    void DogBarking()
    {
        if (myMove.pathContainer.name == "FromBathToBus" && myMove.currentPoint == 18 && busMove.currentPoint == 19 && num_117.activeSelf == true)
        {
            dogBarkingSound.Play();
            Debug.Log("Dog Sound Play");
        }
        if (dogBarkingSound.isPlaying == true)
        {
            if (Input.GetMouseButtonDown(1) || mqttdata.button == 1)
            {
                Debug.Log("Dog checked");
                dog = true;
                mqttdata.button = 0;
            }
        }
    }

    void BusTimerController()
    {
        if (myMove.pathContainer.name == "FromBathToBus" && myMove.currentPoint == 18 && busMove.currentPoint == config.WAYPOINT_BUS_HOME_STOP && num_904.activeSelf == true)
        {
            timerBus.SetActive(true);
            timerIntoBus.StartTimer();
        }
        if (myMove.pathContainer.name == "FromBathToBus" && myMove.currentPoint == 19)
        {
            timerIntoBus.StopTimer();
        }
    }

    void PauseAll()
    {
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    void ShowSignals()
    {
        if (way == true)
        {
            BusStop_Checked.SetActive(true);
        }
        if (dog == true)
        {
            Dog_Checked.SetActive(true);
        }
        if (marketStop == true)
        {
            MarkeStop_Checked.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update () 
	{
		//containerHomeToMarketHandsAnalyzer ();
		containerHomeToMarketSoundsAnalyzer ();
		containerFromMarketStopToMarketSoundsAnalyzer ();
		containerMoveStepAnalyzer ();
		HandsTakerAnalyzer ();
		containerBusStopsAnalyzer();
		PhysicalTakerAnalyzer ();
        BusSwitcher();
        BusSpeedChanger();
        MakeSignal();
        DogBarking();
        ShowSignals();
        BusTimerController();
        if (Input.GetKeyDown(KeyCode.Pause) || Input.GetKeyDown(KeyCode.P))
        {
            PauseAll();
        }
    }
}