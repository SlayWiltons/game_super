﻿using UnityEngine;
using SWS;
using UnityEngine.SceneManagement;

public class WaypointDebuggerGUI : MonoBehaviour
{
	//reference to the movement script
	private splineMove2 myMove;
    private splineMove busMove;    
    private GameObject buscamera;
    private GameObject buscameraIn;
    private GameObject playercamera;
    private GameObject bus;
    private static GeneralConfig config;
    private OpenBusDoor1A busPayDoor;
    public GameObject num_117;
    public GameObject num_904;
    public GameObject num_117_2;
    public GameObject num_904_2;
    public AudioSource prigorod;
    public AudioSource dom;
    public AudioSource pool;
    public AudioSource shop;
    public GameObject Warning;
    public GameObject Player;
    public bool way_flag = false;
    private GameObject InstructionsCamera;
    public GameObject Instructions;
    public GameObject InstructionsController;

    // Use this for initialization
    void Start ()
    {
        busMove = GameObject.Find("EXTE_BUS_STRUCTURE_SepratePartsBUSBUS").GetComponent<splineMove>();
        bus = GameObject.Find("EXTE_BUS_STRUCTURE_SepratePartsBUSBUS");
        myMove = gameObject.GetComponent<splineMove2>();
		myMove.setStartedPath ("HomeToMarket1");
        buscamera = GameObject.FindGameObjectWithTag("BusCamera");
        buscameraIn = GameObject.FindGameObjectWithTag("BusCameraIn");
        playercamera = GameObject.FindGameObjectWithTag("MainCamera");
        Player.GetComponent<mqttTest>().buspay = 0;
        way_flag = true;
        busPayDoor = GameObject.FindGameObjectWithTag("BusDoor").GetComponent<OpenBusDoor1A>();
        InstructionsCamera = GameObject.FindGameObjectWithTag("InstructionsCamera");
    }

	void OnGUI()
    {
        GUI.Box(new Rect(1730, 10, 170, 290), "Меню");
        if (GUI.Button(new Rect(1740, 40, 150, 20), "Начало"))
        {
            SceneManager.LoadScene(1);
        }
        if (GUI.Button(new Rect(1740, 70, 150, 20), "Начало пути"))
        {
            Player.GetComponent<WaypointContainersAnalyzer>().way = false;
            Player.GetComponent<WaypointContainersAnalyzer>().busPay = true;
            busPayDoor.payed = false;
            busPayDoor.cameraSetIntoBus = false;
            NumRevChange();
            CameraSwitch();
            Warning.SetActive(false);
            myMove.SetPath(WaypointManager.Paths["FromBathToBus"]);
            myMove.GoToWaypoint(0);
            myMove.ChangeSpeed(1.5f);
            InstructionsController.GetComponent<SubInstructions>().instr_flag = true;
        }
        if (GUI.Button(new Rect(1740, 100, 150, 20), "Остановка 1"))
        {
            Player.GetComponent<WaypointContainersAnalyzer>().way = false;
            Player.GetComponent<WaypointContainersAnalyzer>().busPay = true;
            busPayDoor.payed = false;
            busPayDoor.cameraSetIntoBus = false;
            NumRevChange();
            CameraSwitch();
            Warning.SetActive(false);
            myMove.SetPath (WaypointManager.Paths["FromBathToBus"]);
			myMove.GoToWaypoint (9);
            myMove.ChangeSpeed(0.7f);
        }
		if (GUI.Button (new Rect (1740, 130, 150, 20), "Остановка 2"))
        {
            Player.GetComponent<WaypointContainersAnalyzer>().way = false;
            Player.GetComponent<WaypointContainersAnalyzer>().busPay = true;
            busPayDoor.payed = false;
            busPayDoor.cameraSetIntoBus = false;
            NumRevChange();
            CameraSwitch();
            Warning.SetActive(false);
            myMove.SetPath (WaypointManager.Paths["FromBathToBus"]); 
            myMove.GoToWaypoint (14);
            myMove.ChangeSpeed(0.7f);
        }
        if (GUI.Button(new Rect(1740, 160, 150, 20), "Подьезжающий автобус"))
        {
            Player.GetComponent<WaypointContainersAnalyzer>().busPay = true;
            busPayDoor.payed = false;
            busPayDoor.cameraSetIntoBus = false;
            StageInit();
            NumRevChange();
            CameraSwitch();
            Warning.SetActive(false);
            busMove.ChangeSpeed(15);
            busMove.GoToWaypoint(17);
            myMove.ChangeSpeed(0.7f);
        }
        if (GUI.Button(new Rect(1740, 190, 150, 20), "Внутри автобуса"))
        {
            Player.GetComponent<WaypointContainersAnalyzer>().busPay = false;
            busPayDoor.payed = false;
            busPayDoor.cameraSetIntoBus = false;
            StageInit();
            NumChange();
            Warning.SetActive(false);
            bus.GetComponent<BusDoorsController>().opened = false;
            bus.GetComponent<BusDoorsController>().StopAllCoroutines();
            busMove.GoToWaypoint(0);
            CameraSwitchToPay();
            myMove.GoToWaypoint(22);
            Debug.Log("Player moved to the point");
            myMove.ChangeSpeed(0.7f);           
        }            
        if (GUI.Button(new Rect(1740, 220, 150, 20), "Выход из автобуса"))
        {
            Player.GetComponent<WaypointContainersAnalyzer>().busPay = true;
            busPayDoor.payed = false;
            busPayDoor.cameraSetIntoBus = false;
            StageInit();
            NumChange();
            Warning.SetActive(false);
            myMove.SetPath(WaypointManager.Paths["FromMarketStopToMarket"]);
            bus.GetComponent<BusDoorsController>().opened = false;
            bus.GetComponent<BusDoorsController>().StopAllCoroutines();
            myMove.GoToWaypoint(0);
            busMove.GoToWaypoint(12);
            myMove.ChangeSpeed(1.5f);
        }
        if (GUI.Button(new Rect(1740, 270, 150, 20), "Инструкция"))
        {
            playercamera.SetActive(false);
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
            }
            InstructionsCamera.SetActive(true);
            Instructions.SetActive(true);
        }
    }

    void StageInit()
    {
        myMove.SetPath(WaypointManager.Paths["FromBathToBus"]);
        Player.GetComponent<WaypointContainersAnalyzer>().way = way_flag;        
        myMove.GoToWaypoint(18);
    }

    void NumChange()
    {
        num_117.SetActive(false);
        num_117_2.SetActive(false);
        num_904.SetActive(true);
        num_904_2.SetActive(true);
    }

    void NumRevChange()
    {
        num_904.SetActive(false);
        num_904_2.SetActive(false);
        num_117.SetActive(true);
        num_117_2.SetActive(true);
    }

    void CameraSwitch()
    {
        buscameraIn.SetActive(false);
        playercamera.SetActive(true);
    }

    void CameraSwitchToPay()
    {
        playercamera.SetActive(false);
        buscameraIn.SetActive(true);
    }
}