﻿using System.Collections.Generic;
using UnityEngine;

public class way : MonoBehaviour {

	public List<Transform> waypoints = new List<Transform>();
	bool isBusy = false;
	int currentStep = 0;

	void Update() {
		if (waypoints != null && !isBusy) {
			Move();
		}
	}

	void Move() {
		isBusy = true;
		Vector3 targetPosition = waypoints[currentStep].position;
		transform.position = Vector3.MoveTowards(transform.position, targetPosition, Time.deltaTime * 10);
		transform.LookAt(waypoints[currentStep]);
		if (Vector3.Distance(transform.position, targetPosition) < 1) {
			isBusy = false;
			currentStep++;
		}
	}
}